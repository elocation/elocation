# 🌍 eLocation

![pipeline](https://gitlab.com/elocation/elocation/badges/development/pipeline.svg)
![release](https://gitlab.com/elocation/elocation/-/badges/release.svg)
![coverage](https://gitlab.com/elocation/elocation/badges/development/coverage.svg)

Secure server that stores your location information. 📍

## Getting started

### Technologies

[![Java 21](https://img.shields.io/badge/Java%2021-FCC624?style=for-the-badge&logo=Oracle&logoColor=black)](https://docs.aws.amazon.com/corretto/latest/corretto-21-ug/what-is-corretto-17.html)
[![Kotlin 1.9.22](https://img.shields.io/badge/kotlin%201.9.22-%237F52FF.svg?style=for-the-badge&logo=kotlin&logoColor=white)](https://kotlinlang.org)
[![Maven](https://img.shields.io/badge/MAVEN-blue?style=for-the-badge&logo=apachemaven&logoColor=white)](https://maven.apache.org/)
[![Spring](https://img.shields.io/badge/Spring%203.2.2-6DB33F?style=for-the-badge&logo=spring&logoColor=white)](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)

### Set up development environment

1. Make sure that you have Java 21 installed and active,
2. Make sure that you have Kotlin 1.9.22 installed and active,

### Run application

1. From main directory run command:

   ```bash
   ./mvnw spring-boot:run
   ```
