package com.cafetamine.elocation

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ELocationApplication

fun main(args: Array<String>) {
    runApplication<ELocationApplication>(*args)
}
