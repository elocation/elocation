package com.cafetamine.elocation

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FooTest {

    @Test
    fun `New Foo should have value of bar`() {
        assertEquals("bar", Foo().value)
    }
}
